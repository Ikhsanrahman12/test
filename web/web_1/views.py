from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
#from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponse, HttpResponseRedirect
from web_1.forms import SignUpForm

# Create your views here.

def signup(request):
	if request.method=="POST":
		form=SignUpForm(request.POST)
		if form.is_valid():
			form.save()
			username=form.cleaned_data.get('username')
			password=form.cleaned_data.get('password')
			raw_password=form.cleaned_data.get('password1')
			user=authenticate(username=username, password=raw_password)
			login(request, user)
			return redirect ('/')
	else :
		form=SignUpForm()
	return render (request, 'signup.html', {'form':form})


def home(requests):
	return HttpResponse ('<h1>you are logged in</h1>')


